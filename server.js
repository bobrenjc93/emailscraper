//Required Libraries ###############################################
var _ = require('underscore'),
    url = require("url"),
    request = require('request'),
    cheerio = require('cheerio');

var logging = false;

//Initialize global functions & events ###############################
var log = function (msg) {
    if (logging) console.log(msg);
};

request.prototype.request = function () {
    this.setMaxListeners(0);
};

//##################################################################

function Scraper() {
    var Emails = [],
        Urls = [],
        Visited = {}, Recursed = {}, errors = {}, emailCalls = 0,
        sitesScraped = 0,
        emailsGrabbed = 0,
        numErrors = 0,
        MAX_SITES_SCRAPED = 100,
        MAX_QUEUED_URLS = 1000;

    this.init = function () {
        process.on('uncaughtException', function (err) {
            if (errors[err]) {
                return;
            } else if (err instanceof Error) {
                log(err.stack);
            } else {
                log(err);
            }
            errors[err] = true;
        });

        process.on('exit', function () {
            console.log('Sites Scraped: ' + (++sitesScraped) + '/' +
                MAX_SITES_SCRAPED + ' Total Emails: ' + Emails.length +
                ' Urls Queued: ' + Urls.length + ' Time Elapsed: ' +
                process.uptime() + ' Errors: ' + numErrors + '\r'
            );
            console.log('');
            console.log('---Results---');
            console.log(_.filter(_.uniq(Emails), function (email) {
                return email.indexOf('jpg') === -1 && email.indexOf('.png') === -1;
            }));
        });

        process.on('SIGINT', function () {
            process.exit();
        });
    };

    this.parseUrl = function (link, host) {
        var lParts = url.parse(link, true),
            hParts = url.parse(host, true);
        if (!lParts.protocol) lParts.protocol = hParts.protocol;
        if (!lParts.host) lParts.host = hParts.host;
        var proto = lParts.protocol.trim();
        if (proto !== 'http:' && proto !== 'https:') return null;
        return url.format(lParts);
    };

    this.grabEmails = function ($, url) {
        var emails = [];
        if ($('html') && $('html').html()) emails = _.without($('html').html().match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi), null);
        Emails = _.union(Emails, emails);
    };

    this.grabUrls = function ($, url) {
        if (sitesScraped < MAX_SITES_SCRAPED && Urls.length < MAX_QUEUED_URLS - 120) {
            var length = $('a').length;
            var links = _.shuffle($('a'));
            for (var i = 0; i < length; i++) {
                if (Urls.length > MAX_QUEUED_URLS) break;
                if (links[i] && links[i].attribs.href) {
                    var href = links[i].attribs.href.trim();
                    var link = this.parseUrl(href, url);
                    if (link && !Visited[link]) Urls.push(link);
                }
            }
        }
        this.recurse(url);
    };

    this.recurse = function (url) {
        if (this.filterUrl(Urls.slice(0, 1)[0]))
            return this.recurse(Urls.splice(0, 1)[0]);
        if (sitesScraped < MAX_SITES_SCRAPED && Urls.length) {
            return this.scrapeSite(Urls.splice(0, 1)[0]);
        } else {
            process.exit();
        }
    };

    this.filterUrl = function (url) {
        var bad = ['.zip', '.pdf', '.mp3', '.jpg', '.rar', '.exe', '.wmv', '.avi', '.doc', '.ppt', '.xls'];
        for (var i = 0; i < bad.length; i++) {
            if (url.indexOf(bad[i]) !== -1)
                return true;
        }
        return false;
    }

    this.scrapeSite = function (url, recursive) {
        var that = this;
        if (!Visited[url] && url) {
            Visited[url] = true;
            var timer = new Date().getTime() / 1000;
            request({
                url: url,
                timeout: 5000
            }, function (err, response, body) {
                console.log((new Date().getTime() / 1000 - timer) + " " + url);
                if (Recursed[url]) {
                    return; //Second time callback has been called for particular request
                }
                Recursed[url] = true;
                if (err) return that.catchErr(err, url);
                try {
                    var $ = cheerio.load(body);
                } catch (err) {
                    that.catchErr(err, url);
                }
                var stats = 'Sites Scraped: ' + (++sitesScraped) + '/' + MAX_SITES_SCRAPED + ' Total Emails: ' + Emails.length +
                    ' Urls Queued: ' + Urls.length + ' Time Elapsed: ' + process.uptime() + ' Errors: ' + numErrors +
                    ' Memory Used: ' + JSON.stringify(process.memoryUsage()) + '\r';
                if (logging) {
                    log(stats);
                } else {
                    //process.stdout.write(stats);
                    console.log(stats);
                }
                if ($) {
                    that.grabEmails($, url);
                    that.grabUrls($, url);
                } else {
                    that.recurse(url);
                }
            });
        } else {
            that.recurse(url);
        }
    };

    this.catchErr = function (err, url) {
        this.recurse(url);
        throw err;
    };
}

var scraper = new Scraper();
scraper.init();
scraper.scrapeSite('http://crunchbase.com');